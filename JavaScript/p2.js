llamandoFetch = ()=>{
    const ele = "https://restcountries.com/v3.1/name/"
    let pais = document.getElementById("pais").value
    const url = ele + pais   
    fetch(url)
    .then(respuesta => respuesta.json())
    .then(data => mostrardatos(data))
    .catch((reject)=> {
        console.log("surgio un error " + reject);
    });
}
const mostrardatos=(data)=>{
    console.log(data)
    const respuesta1 = document.getElementById('respuesta1');
    const respuesta2 = document.getElementById('respuesta2');
    respuesta1.innerHTML = "";
    respuesta2.innerHTML = ""

    for(let item of data){
        respuesta1.innerHTML= item.capital
        const lenguaje = Object.values(item.languages)
        respuesta2.innerHTML = lenguaje
    }
}
document.getElementById("BUSCAR").addEventListener("click",function(){
    llamandoFetch();
})