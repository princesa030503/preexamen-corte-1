document.addEventListener("DOMContentLoaded", function() {
    const idInput = document.getElementById("ID");
    const nombreInput = document.getElementById("nombre");
    const nombreUsuarioInput = document.getElementById("nombre usuario");
    const emailInput = document.getElementById("Email");
    const calleInput = document.getElementById("calle");
    const numeroInput = document.getElementById("numero");
    const ciudadInput = document.getElementById("ciudad");
    const mostrarButton = document.getElementById("mostrar");

    // Manejador de evento para el clic en el botón "mostrar"
    mostrarButton.addEventListener("click", function() {
      // Obtén el valor del campo ID
      const userId = idInput.value;

      // Realiza la solicitud a la API con el ID proporcionado
      fetch(`https://jsonplaceholder.typicode.com/users/${userId}`)
        .then(response => response.json())
        .then(data => {
          // Actualiza los campos del HTML con los datos recibidos
          nombreInput.value = data.name;
          nombreUsuarioInput.value = data.username;
          emailInput.value = data.email;
          calleInput.value = data.address.street;
          numeroInput.value = data.address.suite;
          ciudadInput.value = data.address.city;
        })
        .catch(error => console.error('Error al obtener datos:', error));
    });
  });
